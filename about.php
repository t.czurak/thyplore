      <!-- START THE FEATURETTES -->
     <div class="container marketing" style="padding:40px;">
      <hr>
      <div class="row featurette">
        <div>
        <div style="float:right;"><img class=" img-circle" style="width:200px" src="/img/bino.jpg"></div>
          <h2 class="featurette-heading">What is ThyPlore.com?</span></h2>
          <p class="lead">ThyPlore.com is a local search app based on Wikipedia. It automatically detects your location and shows Wikipedia articles within a 400m/1,300 feet radius. Clicking on a marker on the map displays an infowindow showing:
          <ul>
          <li>an excerpt from Wikipedia,</li>
          <li>a thumbnail image,</li>
          <li>a direct link to the Wikipedia article.</li>
          </ul>
          </p>
          <hr/>
          <h2>In what situations can I use this app?</h2>
          <p>The most obvious answer is that ThyPlore.com can prove useful and fun when exploring new areas, for example when sightseeing, cycling or traveling by car. If you suddenly see an interesting building or monument around you, you can bet that ThyPlore.com can tell you what it is.</p>
          <hr/>
          <h2>What about my privacy?</h2>
          <p>ThyPlore.com does not store any location data of its users (apart from what Google Analytics snippet is tracking, but that's up to guys at Google, not the author of this site).</p>
          <hr/>
          <h2>I love this app! Is there anything I can do to help you?</h2>
          <p>You could begin with sharing this website on social media. For your convenience I have placed sharing buttons below. </p>
          <!-- Go to www.addthis.com/dashboard to customize your tools -->
          <div class="addthis_sharing_toolbox"></div>
          <hr/>
          <h2>Contact the author</h2>
          <p>If you'd like to contact me, go to <a href="http://www.iamtomek.com/contact-me">www.iamtomek.com/contact-me</a> and use the contact form or send me an e-mail. </p>
            <hr/>
          <p style="text-align:center"><a href="/"><span style="font-size: 20px;" class="btn btn-success" role="button">Click here to see what's around you</span></a></p>
          </hr>          
          </div>
      </div>

      <hr class="featurette-divider">

      <!-- /END THE FEATURETTES -->


      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#navbar">| Back to top</a></p>
        <p>&copy; 2015 | This app is brought to you by <a href="http://www.iamtomek.com">www.iamtomek.com</a></p>
      </footer>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
    <script src="http://www.thyplore.com/js/tomek-script.js"></script>
    
  <!------ facebook SDK -->
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=393636937372332&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
  <!-- facebook SDK -->
    
      
  </body>
</html>
