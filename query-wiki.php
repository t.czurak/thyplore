<?php
//error_reporting(0); //supress errors so that JSON reposnse does not get broken

if ($_GET["markers"]) { 
	$wikiApi = new wikiApi;
	$wikiApi->getPlaces();
	$wikiApi->sendJSON();
}

if ($_GET["infowindow"]) { 
	$pageid = $_GET["pageid"];
	$wikiApi = new wikiApi;
	$wikiApi->getThumb($pageid);
	$wikiApi->getUrl($pageid);
	$wikiApi->getExtract($pageid);
	$wikiApi->sendJSON();
}

class wikiApi {
	
	public $exported_places = array(); //the main object that willl be exported to the client, contains only the necessary data
	public $radius = 400;
	public $pageids,  $lat, $lon, $lang, $format, $extract;
	
	public function __construct() {
		$this->lat = $_GET["lat"];
		$this->lon = $_GET["lon"];
		$this->lang = $_GET["userLangCode"];
		if ($_GET["json"] == "1") { $this->format = "&format=json"; }
	}
	
	public function getPlaces() {
		//get places around the radius
		$query = "http://{$this->lang}.wikipedia.org/w/api.php?action=query&list=geosearch&gslimit=50&gsradius={$this->radius}&gscoord={$this->lat}|{$this->lon}&prop=coordinates{$this->format}";
		$titles_cords = json_decode(file_get_contents($query)); //now we have got: lat, lon, title, pageid
		$result_set = $titles_cords->query->geosearch;
	
		//if no locations found around given radius
		if (empty($result_set)) {
			echo "none";
			exit;
		}
		
		//prepare pageids string for future queries
		foreach ($result_set as $single_location) {
			$id = $single_location->pageid;
			$pageids .= $id . "|";
		}
		$this->pageids = trim($pageids , "|");
			
		//populate $exported_places object
		foreach ($result_set as $single_location) {
			$this->exported_places[$single_location->pageid] = array(
				"pageid" =>  $single_location->pageid,
				"title" =>  $single_location->title,
				"lat" =>  $single_location->lat,
				"lon" =>  $single_location->lon,
				"dist" =>  $single_location->dist
				);
		}
	}
	
	public function getThumb($pageid) {
		$query = "http://{$this->lang}.wikipedia.org/w/api.php?action=query&prop=pageimages&pageids={$pageid}&piprop=thumbnail&pithumbsize=400{$this->format}";
		$thumbnails = json_decode(file_get_contents($query)); //we have got: thumbnails
		
		//populate $exported_places object with thumbnails
		$result_set = $thumbnails->query->pages;
		foreach ($result_set as $single_location) {
			$the_array =& $this->exported_places[$single_location->pageid];
			
			if ($single_location->thumbnail->source) {
				$the_array["thumbnail"] =  $single_location->thumbnail->source;
			} else {
				//if no thumbnail, get first article image
				$the_array["thumbnail"] = $this->getImage($single_location->pageid);
			}
		}
	}
	
	private function getImage($pageid) {
		$query = "http://{$this->lang}.wikipedia.org/w/api.php?action=query&prop=images&pageids={$pageid}{$this->format}";
		//echo $query . "\n";
		$result = json_decode(file_get_contents($query));
		$images = $result->query->pages->$pageid->images;
		
		//ignore .svg files
		foreach ($images as $image) {
			$title = $image->title;
			if (!stristr($title, ".svg")){
				$image_title = $title;
				break;
			}
		}
		//if only .svg files, take the first one
		if (!$image_title) { $image_title = $images[0]->title; }
		
		$image_title = urlencode($image_title);
		$query = "http://{$this->lang}.wikipedia.org/w/api.php?action=query&titles={$image_title}&prop=imageinfo&iiprop=url{$this->format}";
		$result = json_decode(file_get_contents($query));
		$image_url = $result->query->pages->{-1}->imageinfo[0]->url;

		return $image_url;
	}
	
	public function getUrl($pageid) {
		$query = "http://{$this->lang}.wikipedia.org/w/api.php?action=query&pageids={$pageid}&prop=info&inprop=url{$this->format}";
		$links = json_decode(file_get_contents($query)); //we have got: thumbnails
				
		//populate $exported_places object with urls
		$result_set = $links->query->pages;
		foreach ($result_set as $single_location) {
			$the_array =& $this->exported_places[$single_location->pageid];
			$the_array["fullurl"] =  $single_location->fullurl;	
		}
	}
	
		public function getExtract($pageid) {
		$query = "http://{$this->lang}.wikipedia.org/w/api.php?action=query&prop=extracts&pageids={$pageid}&exchars=400&explaintext{$this->format}";
		$extracts = json_decode(file_get_contents($query)); //we have got: extracts
		
		//populate $exported_places object with extracts
		$result_set = $extracts->query->pages;
		foreach ($result_set as $single_location) {
			$the_array =& $this->exported_places[$single_location->pageid];
			if ($single_location->extract) {
				$the_array["extract"] =  $single_location->extract;
			} else {
				$the_array["extract"] = "none";
			}
		}
	}
	
	public function sendJSON() {
		echo json_encode($this->exported_places);
		exit;
	}
	
} //end class
?>