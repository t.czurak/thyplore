var lat, lon, userLangCode;
var map, mapOptions, myLatlng=0;
var pageids = "";
var images = { }; //object

//display the map
function initialize() {
if (myLatlng == 0) {
	var mapOptions = {
	    zoom: 5,
	    center: {lat: 34.052234, lng: -118.243684}
	  };
	map = new google.maps.Map(document.getElementById('map-canvas'),  mapOptions);
}
}
	google.maps.event.addDomListener(window, 'load', initialize);


/*get location */
if(geo_position_js.init()){
	geo_position_js.getCurrentPosition(success_callback,error_callback,{enableHighAccuracy:true});
} else {
	showManualInput();	
}

function success_callback(p) {
lat = p.coords.latitude.toFixed(8);
lon = p.coords.longitude.toFixed(8);
var lats = lat + "; " + lon;
displayLats(lats);
showGoButton();
}

function error_callback(p) {
console.log('error='+p.code);
showManualInput();
}

/*unravel input for manual location entry */
var gobutton = document.getElementById("manual");
function showManualInput() {
$('#manual').hide( "slow");
$('#manual-input').show( "fast");
}
gobutton.addEventListener("click", function() {
showManualInput();
});

/*hide manual address input */
function hideManualInput() {
$('#manual-input').hide( "slow");
}

/*populate the map ("show me what's round" button clicked) */
var gobutton = document.getElementById("go");
gobutton.addEventListener("click", function() {
	
	//show ajax loader
  loader("show");
	
	//Scroll to the map
	document.getElementById("your-location").scrollIntoView();
	
	//read user locale
	var Lang = document.getElementById("user_locale");
	var userLang = Lang.getAttribute("user_lang");
	userLangCode = Lang.getAttribute("user_lang_code");
	
	//send AJAX request
	$.get( "/query-wiki.php", {
	"markers":"true",
	"json":1,
	"lat": lat,
	"lon":lon,
	"userLang":userLang,
	"userLangCode":userLangCode
	}, function( resp ) {
	displayPlaces( resp ); // server response
	
	//hide ajax loader
	loader("hide");
	
	});

});

/* unravel "show me what's round" button */
function showGoButton() {
$('#go').show( "fast");	
}

/*display coordinates */
function displayLats(lats) {

var content = document.createTextNode(lats);
var newNode = document.createElement("span");
newNode.setAttribute("id", "lats");
newNode.appendChild(content);

var parent = document.getElementById("your-location");
var child = document.getElementById("lats");

parent.replaceChild(newNode, child);

//drop the "I'm here" marker on the map
map = new google.maps.Map(document.getElementById('map-canvas'),  mapOptions);
google.maps.event.addListenerOnce(map, 'idle', function(){ IamHere(); });
}

/*encode coorfinates from manual addres entry */
var myAddress = document.getElementById("myAddress");

myAddress.addEventListener("keydown", function(event) {
	if ((event.which || event.keyCode) == 13) {
		
		geocoder = new google.maps.Geocoder();
		geocoder.geocode( { 'address': String(myAddress.value)}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
		var cordObj = results[0].geometry.location;
		lat = cordObj["G"];
		lon = cordObj["K"];
		
		var lats = lat + "; " + lon;
		displayLats(lats);
		showGoButton();
		hideManualInput();
		}});
	}
});

//show the "I'm here" marker
function IamHere() {
	
	myLatlng = new google.maps.LatLng(lat,lon);
	var mapOptions = {
	  zoom: 16,
	  center: myLatlng
	}
	map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
	
	
	var marker = new google.maps.Marker({
	  icon: {
	  path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
	  scale: 9
	  },
	    position: myLatlng,
	    map: map,
	    animation: google.maps.Animation.DROP,
	    title:"You are here!"
	});
	
	 var infowindow = new google.maps.InfoWindow({
	 content: "<h3>You are here!</h3>"
          	});  
                      
         	google.maps.event.addListener(marker, 'click', function() {
         	infowindow.open(map,marker);
         	});
	
	marker.setAnimation(google.maps.Animation.BOUNCE);
}

//display markers with places
function displayPlaces(places) {
	
	if (places == "none") {
	
    //read user locale
    var Lang = document.getElementById("user_locale");
    var userLang = Lang.getAttribute("user_lang");

		alert("No Wiki articles have been found near you. You can try changing Wiki language (see top menu) to get more results." + "Your current language: " + userLang + " \n\nIf it doesn't help, move to a more interesting area and try again. Good luck!");
		
	} else {
		var places = JSON.parse(places);
		for (var key in places) { ////iterate through array of objects
			
			var thislat = places[key]["lat"];
			var thislon = places[key]["lon"];
			var title = places[key]["title"];
			var pageid = places[key]["pageid"];
			
			displayInfowindow(thislat,thislon, pageid, title);
		}
	}
	
	//display the circle
	var myLatlng = new google.maps.LatLng(lat,lon);
	var circleOptions = {
	strokeColor: '#001eff',
	strokeOpacity: 0.8,
	strokeWeight: 1,
	fillColor: '#00FF00',
	fillOpacity: 0.20,
	map: map,
	center: myLatlng,
	radius: 400
	};
	
    	// Add the circle for this city to the map.
    	var cityCircle = new google.maps.Circle(circleOptions);
}

function displayInfowindow(lat, lon, pageid, title) {
	
	var position = new google.maps.LatLng(lat,lon);
     
	var marker = new google.maps.Marker({ 
			map: map,
			position: position,
			title:  String(title)
          	});
                         
      google.maps.event.addListener(marker, 'click', function() {
      
      loader("show");
        			
      var resp;
			//send AJAX request
			$.get( "/query-wiki.php", {
			"infowindow":"true",
			"json":1,
			"userLangCode":userLangCode,
			"pageid":pageid
			}, function( resp ) {
			
			var places = JSON.parse(resp);
			for (var key in places) { ////iterate through array of objects
					
				var thumb = places[key]["thumbnail"];
				if (thumb == "none") { var disp_thumb = ""; } else { var disp_thumb = "<p style='background-color:#ddd'><img style='height:200px;' src='" + thumb + "'></p>"; }
					
				var extract = places[key]["extract"];
				if (extract == "none") { var disp_extract = ""; } else { var disp_extract = "<p style='font-weight:400;'>" + extract + "</p>"; }
				
				var url = places[key]["fullurl"];
					
				var info = "<div class='markerinfo';><h3>";
				info += title + "</h3>" + disp_thumb;
				info += disp_extract;
				info += "<p role=\"button\" class=\"btn btn-success\"><a style='color:#fff; text-decoration:none; !important' target='_BLANK' href='" + url + "'>More on Wikipedia</a></p><br/>";
				info += "<div>";
			}
			
        			var infowindow = new google.maps.InfoWindow({
        			content: info
        			});  
        			infowindow.open(map,marker);
			
             loader("hide");
             
			});
        	});
    }
    
    
    //show ajax spinner gif
    function loader(action) {
      if (action == "show") {
        $('#marker-loader').show();
        $('#fog').show();
      }
    
      if (action == "hide") {
        $('#marker-loader').hide();
        $('#fog').hide();
      }
    }


