<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/img/favicon.png">

    <title>Explore your surroundings | Wikipedia powered local search | ThyPlore.com</title>

    <!-- Bootstrap core CSS -->
    <link href="https:////maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
    <script src="js/ie-emulation-modes-warning.js"></script>
    <!-- gelocation library -->
    <script src="http://code.google.com/apis/gears/gears_init.js" type="text/javascript" charset="utf-8"></script>
    <script src="http://www.thyplore.com/js/geo/geo.js?id=1" type="text/javascript" charset="utf-8"></script>
    
    <!-- gmaps api -->
     <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAoG2FX8Hgp51w6yJFIZdDZ8JvcCKnzyqE"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="/css/carousel.css" rel="stylesheet">
    <link href="/css/tomek-style.css" rel="stylesheet">
    
  </head>
<!-- NAVBAR
================================================== -->
  <body>
  
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1881140-31', 'auto');
  ga('send', 'pageview');
  </script>
  
  <!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-551be91d30d28e9b" async="async"></script>
  
  <div class="navbar-wrapper">
      <div class="container">

        <nav class="navbar navbar-inverse navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="/">ThyPlore.com</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="/">Home</a></li>
                <li><a href="?about=1">About</a></li>
               
               <!-- lang navbar -->
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Wiki language <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#"><strong><?php echo $user_lang; ?></strong> <?php echo $lang_origin; ?></a></li>
                    <li class="divider"></li>
                    <li class="dropdown-header">Pick a different language</li>
                    <li><a href="?user_lang=English&user_lang_code=en&user=1">English</a></li>
                    <li><a href="?user_lang=Spanish&user_lang_code=es&user=1">Spanish</a></li>
                    <li><a href="?user_lang=German&user_lang_code=de&user=1">German</a></li>
                    <li><a href="?user_lang=Chinese&user_lang_code=zh&user=1">Chinese</a></li>
                    <li><a href="?user_lang=Portuguese&user_lang_code=pt&user=1">Portuguese</a></li>
                    <li><a href="?user_lang=Russian&user_lang_code=ru&user=1">Russian</a></li>
                    <li><a href="?user_lang=Japanese&user_lang_code=jp&user=1">Japanese</a></li>
                    <li><a href="?user_lang=French&user_lang_code=fr&user=1">French</a></li>
                    <li><a href="?user_lang=Italian&user_lang_code=it&user=1">Italian</a></li>
                    <li><a href="?user_lang=Polish&user_lang_code=pl&user=1">Polish</a></li>
                  </ul>
                </li>
               <!-- lang navbar --> 
               <span class="hidden" id="user_locale" user_lang="<?php echo $user_lang; ?>" user_lang_code="<?php echo $user_lang_code; ?>"></span>
                              
              </ul>
            </div>
          </div>
        </nav>

      </div>
    </div>