<?php
class userLocale {
	public $lang_code;
	public $lang_word;
	private $lang_codes = array(
		"en" => "English",
		"es" => "Spanish",
		"de" => "German",
		"zh" => "Chinese",
		"pt" => "Portuguese",
		"ru" => "Russian",
		"jp" => "Japanese",
		"fr" => "French",
		"it" => "Italian",
		"pl" => "Polish"
		);		
	
	function __construct() {
		$langs_arr = explode(";", $_SERVER["HTTP_ACCEPT_LANGUAGE"]);
		$langs_arr = explode(",", $langs_arr[0]);
		
		if (stristr($langs_arr[0], "-")) {
			$langs_arr = explode("-", $langs_arr[0]);	
		}
		
		$this->lang_code =$langs_arr[0]; 
		$this->lang_decode();
	}
	
	private function lang_decode() {
		foreach ($this->lang_codes as $key=>$value) {
			if (stristr($this->lang_code, $key)) {
				$this->lang_word = $value;
			}
		}
		//default value if not found
		if (empty($this->lang_word)) {
			$this->lang_word = "English";
			$this->lang_code = "en";
		}
	}
}

$userLocale = new userLocale();

if ($_GET["user_lang"]) { $user_lang = $_GET["user_lang"]; } else { $user_lang = $userLocale->lang_word; }
if ($_GET["user_lang_code"]) { $user_lang_code = $_GET["user_lang_code"]; } else { $user_lang_code = $userLocale->lang_code; }
if ($_GET["user"]) { $lang_origin = "(user selected)"; } else { $lang_origin = "(from your browser settings)"; }

include("header.php");

if ($_GET["about"]) {
	include("about.php");
} else {
	include("frontpage.php"); 
}
?>