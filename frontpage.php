
    <div class="infowindow">
    
    
     <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-4">
          <img class="img-circle info-icon" src="/img/bino.jpg" >
          <h2>Wiki powered local search</h2>
          <p>If you've ever wondered what <strong>Wikipedia</strong> has to tell you about what's around you, now you'll now. ThyPlore.com gets your <strong>current location</strong> and shows <strong>Wikipedia articles</strong> from your immediate surroundings.</p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle info-icon" src="/img/location.jpg">
          <h2>Step 1 - share  location</h2>
          <p>To use this app to its fullest potential, enable your device's <strong>GPS positioning</strong> and all location services. When the browser dialog asks you to <strong>share your location</strong> with ThyPlore.com, click "<strong>Always allow</strong>".</p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle info-icon" src="/img/map.jpg">
          <h2>Step 2 - explore the map</h2>
          <p><strong>Click on the marker</strong> to see a Wiki excerpt from an article. You can then click the link and go <strong>directly to Wikipedia</strong> to get more information on a place. Note: you are presented with Wiki articles <strong>within a 400m radius</strong> from your current location.</p>
        </div><!-- /.col-lg-4 -->
        <p id="your-location"><span>Your location: </span><span id="lats">not known yet</span></p>
        <p></p>
        <p><span id="go"  style="font-size: 20px;" class="btn btn-danger" role="button">Show me what's around!</span></p>
        <p></p>
        <p id="manual"><small>If auto-location doesn't work, <span style="text-decoration:underline"><strong>click here</strong></span> to enter your location manually.</small></p>
        <p id="manual-input"><input type="text" id="myAddress" class="form-control" placeholder="Enter yout location, eg: 100 Rodeo Dr Los Angeles, CA 90035, USA"></p>
        <div id="fog"><img src="/img/ajax-loader-dark.gif" id="marker-loader"></div>
      </div><!-- /.row -->
      
      </div>
    
    </div>
      <div class="map-cont">
    <img src="/img/scrollbar.jpg" class="hidden-lg" style="float:right">
      <div id="map-canvas"></div>
      </div>
      <!-- START THE FEATURETTES -->
     <div class="container marketing">
      <hr>
      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">Do you like this app? <span class="text-muted">Share it on Facebook.</span></h2>
          <p class="lead">Click the buttons below to share this website on Facebook and other social media. Let your buddies have fun too!</p>
         
          <!-- Go to www.addthis.com/dashboard to customize your tools -->
          <div class="addthis_sharing_toolbox"></div>
          
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-circle info-icon visible-lg" src="/img/facebook.jpg">
        </div>
      </div>

      <hr class="featurette-divider">

      <!-- /END THE FEATURETTES -->


      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#navbar">| Back to top</a></p>
        <p>&copy; 2015 | This app is brought to you by <a href="http://www.iamtomek.com">www.iamtomek.com</a></p>
      </footer>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
    <script src="http://www.thyplore.com/js/tomek-script.js"></script>
    
  <!------ facebook SDK -->
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=393636937372332&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
  <!-- facebook SDK -->
    
      
  </body>
</html>
